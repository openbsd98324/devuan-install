# devuan-install -- AMD64 ASCII BASE System. 

A system to boot LIVE (toram) with a rootfs /DEVUAN-BASE.tar.gz (amd64, ascii) and the included custom-installer. 

# Prepare the LIVE stored on the Partition sda2. 

cd /

mkdir devuan-live

cd devuan-live

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/release-v5/initrd.img"

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/release-v5/vmlinuz"

wget -c  --no-check-certificate   https://gitlab.com/openbsd98324/devuan-ascii/-/raw/master/architecture/amd64/1630223889-devuan-rootfs-live-ascii-with-base-v2.squashfs  -O devuan.squashfs

# Boot with grub


menuentry 'RAMDISK Devuan Live Installation, Standard, from sda2' {

 set root='hd0,msdos2'

 linux  /devuan-live/vmlinuz  boot=live toram=devuan.squashfs  live-media-path=devuan-live  bootfrom=/dev/sda2   
 
 initrd /devuan-live/initrd.img

}


# Checksum


devuan-live$ md5sum * 

7193b5eaca13713679833b9ec45f5910  devuan.squashfs

255232c122225ba83742fb59bf3e9cfe  initrd.img

f55229e9a22088ecfb751f359880cbd9  vmlinuz







